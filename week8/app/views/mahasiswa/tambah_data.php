<div>
    <form action="<?= BASEURL ?>Mahasiswa/tambah" method="post">
        <div class="row">
            <div class="col-6">


                <div class="form-group">
                    <label>NIM</label>
                    <input type="text" class="form-control" placeholder="nim" name="nim">
                </div>
                <div class="form-group">
                    <label>Nama</label>
                    <input type="text" class="form-control" placeholder="nama" name="nama">
                </div>
                <div class="form-group">
                    <label>Alamat</label>
                    <input type="text" class="form-control" placeholder="alamat" name="alamat">
                </div>
                <div class="form-group">
                    <label>Prodi</label>
                    <select class="form-control" name="prodi">
                        <option value="">-Pilih Prodi-</option>
                        <option value="teknik informatika">Teknik Informatika</option>
                        <option value="sistem informasi">Sistem Informasi</option>
                        <option value="manajemen">Manajemen</option>
                    </select>
                </div>

                <button type="submit" class="btn btn-primary">Submit</button>
            </div>

        </div>
    </form>
</div>