<?php

class Mahasiswa_model
{
    private $db;

    public function __construct()
    {
        $this->db = new Database;
    }


    public function tambah($data)
    {
        $query = "INSERT INTO mahasiswa VALUES ('', :nim, :nama, :alamat, :prodi)";

        $this->db->query($query);
        $this->db->bind('nim', $data['nim']);
        $this->db->bind('nama', $data['nama']);
        $this->db->bind('alamat', $data['alamat']);
        $this->db->bind('prodi', $data['prodi']);

        $this->db->execute();

        return $this->db->rowCount();
    }
    public function tampil()
    {
        $this->db->query("SELECT * FROM mahasiswa ORDER BY id DESC");
        return $this->db->resultSet();
    }
}
